/*
On utilise les fonctions de base de SDL version 2 (SDL.h) ainsi que des fonctionnalités supplémentaires
permettant de charger des images dans des formats autres que bmp (SDL_image.h), de jouer des musiques
et des sons (SDL_mixer.h) ainsi que d'écrire à l'écran (SDL_ttf.h).

On inclue tous les .h des modules nécessaires (menu, palet, maillet, charger) ainsi qu'un .h contenant 
des constantes (dimensions de l'écran de jeu, etc)

*/

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>

#include "constantes.h"
#include "charger.h"
#include "menu.h"
#include "palet.h"
#include "maillet.h"

/* On déclare fenêtreAirHockey, pointeur sur la structure SDL_Window fournie par SDL. 
On pourra alors créer notre fenêtre de jeu. */
SDL_Window* fenetreAirHockey = NULL; 


/* On déclare un pointeur sur SDL_Surface, structure fournie par SDL et qui permet 
de stocker une image ou dessiner; ecran sera l'image que nous pourrons voir dans la fenêtre de jeu. */
SDL_Surface* ecran = NULL; 


/* *** FONCTION PRINCIPALE *** */

int main(int argc, char *argv[]){

	/*On initialise SDL et ce qui permet d'afficher des images jpg et png à l'écran.
	En effet, sans initialisation, on ne peut pas utiliser SDL. */
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);
	IMG_Init(IMG_INIT_JPG|IMG_INIT_PNG);


	/* *** TEXTE *** */

	// Ce qui suit servira pour afficher les scores à l'écran.
	TTF_Font* police = NULL; // police pointera sur la police que l'on choisira.
	TTF_Init();  // initialisation de ce qui permet d'écrire à l'écran directement, grâce à SDL.
	police = TTF_OpenFont("../graphismes/polices/pol1.ttf", 30); // On choisit notre police.
	SDL_Color couleur = {255,0,0}; // On prépare la couleur de notre texte: rouge.


	/* *** SON *** */

	// Ce qui suit servira pour la musique lorsque l'application sera en cours d'utilisation.
	// Initialisation avec les paramètres fréquence, format, 2 pour stéréo, taille du "chunk".
	Mix_OpenAudio(44100,MIX_DEFAULT_FORMAT,2,2048);
	//Mix_Music *musique; // musique pointera sur le fichier mp3 choisi.
	Mix_Chunk *goal = Mix_LoadWAV("../sons/goal2.wav"); // Effet sonore lié à un but chargé grâce à Mix_LoadWAV.

	/* *** FENETRE ET ECRAN *** */

	/* On crée la fenêtre de jeu grâce à une fonction fournie par SDL, SDL_CreateWindow.
	Le 1er paramètre est le nom de la fenêtre qui apparait dans la barre tout en haut.
	Le 2 suivants sont l'abscisse et l'ordonnée du sommet en haut à gauche de la fenêtre.
	Les valeurs par défaut SDL_WINDOWPOS_UNDEFINED permettent de centrer la fenêtre sur 
	l'écran de notre ordinateur. Ensuite les dimensions de la fenêtre.  
	Finalement, un drapeau indiquant qu'on souhaite effectivement 
	afficher cette fenêtre sur l'écran de notre	ordinateur. */
	fenetreAirHockey = SDL_CreateWindow("Air Hockey", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, ECRAN_LONGUEUR, ECRAN_LARGEUR, SDL_WINDOW_SHOWN);

	/* Pour qu'ecran soit effectivement l'image que l'on voit au sein de la fenêtre de jeu, on doit le 
	préciser avec la fonction SDL_GetWindowSurface qui associe la surface ecran à fenetreAirHockey. */
	ecran = SDL_GetWindowSurface(fenetreAirHockey);


	/* *** GESTION DE LA FREQUENCE DES RENDUS *** */

	/* On prépare le terrain pour imposer la cadence de rafraichissement des images ici.
	On finalisera à la fin de la boucle principale du jeu en utilisant la fonction SDL_GetTicks et SDL_Delay.
	SDL_GetTicks, au moment de son appel, fournie le temps depuis l'initialisation de SDL.
	SDL_Delay correpond au fait d'attendre un certain temps 
	avant de poursuivre avec les prochaines instructions. */
	const int FPS = 60; // On souhaite imposer une cadence de 60 images par seconde.
	/* Cette variable (entier non signé sur 32 bits) marquera l'instant de départ de la boucle principale du jeu. */
	Uint32 start; 


	/* *** CREATION DES OBJETS ET DECLARATIONS	  *** */

	// Image du terrain chargée grâce à la fonction chargerImage qu'on a définie dans le module charger.
	SDL_Surface* terrain = chargerImage(ecran, "../graphismes/fonds/terrain.jpg");
	SDL_Surface* imgGoal = chargerImage(ecran, "../graphismes/goal.jpg"); // Image qui s'affiche quand il y a but.

	// Les deux maillets et le palet.
	Maillet joueur1(chargerImage(ecran, "../graphismes/mailletsPalet/maillet1.png"), 33, ECRAN_LARGEUR/2);
	Maillet joueur2(chargerImage(ecran, "../graphismes/mailletsPalet/maillet2.png"), 607, ECRAN_LARGEUR/2);
	Palet palet(chargerImage(ecran, "../graphismes/mailletsPalet/palet.png"), 319.5, ECRAN_LARGEUR/2, 0, 0);

	// Barrière centrale qui est un rectangle; sera utile pour le confinement des maillets dans leurs zones.
	SDL_Rect barre;
	barre.x = 315;
	barre.y = 0;
	barre.w = 9;;
	barre.h = ECRAN_LARGEUR;

	// Drapeau pour gérer la rentrée dans la boucle principale du jeu.
	bool jeuEnCours = false;


	/* *** MENU PRINCIPAL *** */

	/* Affiche le menu principal et permet de naviguer dans les sous menus Options et Instructions.
	Il s'agit d'une boucle infinie de laquelle on sort quand on clique sur l'un des boutons.
	On renvoie alors le numéro du bouton. 
	Dans la suite, on a besoin de traiter que le cas où on clique sur Jouer et on renvoie 0.
	Les autres cas sont traités au sein de la fonction. Le cas Quitter laisse le booléen jeuEnCours à false.*/
	int i = afficherMenuPrincipal(ecran, fenetreAirHockey); 

	// On demande à commencer le jeu.
	if(i == 0){
		jeuEnCours = true;
        //musique=Mix_LoadMUS("../sons/menup3.mp3"); // On charge une musique.
        //Mix_PlayMusic(musique,-1); // On lance la musique en boucle grâce à l'argument -1.
    }


	// Pour gérer les événements dans la suite.
	SDL_Event e;


	/* *** BOUCLE PRINCIPALE DU JEU *** */

	while(jeuEnCours){

		start = SDL_GetTicks(); // On marque l'instant en ms auquel on rentre dans cette boucle.


		/* *** GESTION DES EVENEMENTS *** */

		// On reste dans cette boucle tant qu'il y a un événement dans la file d'attente.
		while(SDL_PollEvent(&e)){ // On extrait tour à tour les événements dans la file d'attente. Priorité FIFO.
			switch(e.type){ // On effectue des tests selon la nature de l'événement.
				case SDL_QUIT: // On a appuyé sur la croix de la fenêtre.
					jeuEnCours = false; // JeuEnCours est mis à false et on sortira de cette boucle.
				break;

				// A l'appui d'une touche.
			    case SDL_KEYDOWN:
			        switch(e.key.keysym.sym)
			        {
			            /* Pour le joueur 1.
			            Si on veut monter avec la touche z, l'axe des ordonnées étant orientée vers le bas,
			         	on initialise la vitesse de ce joueur selon cette axe de sorte qu'à l'appel
			         	de la fonction pour le déplacer, il se déplace effectivement vers le haut.
			         	On fait ça pour les deux axes et les deux sens. */
			            case SDLK_z: joueur1.dimVitY(); break; // Pour monter.
			            case SDLK_s: joueur1.augVitY(); break; // Pour descendre.
			            case SDLK_q: joueur1.dimVitX(); break; // Pour aller à gauche.
			            case SDLK_d: joueur1.augVitX(); break; // Pour aller à droite.
			            // Même chose pour le joueur 2.
			            case SDLK_UP: joueur2.dimVitY(); break; // Pour monter.
			            case SDLK_DOWN: joueur2.augVitY(); break; // Pour descendre.
			            case SDLK_LEFT: joueur2.dimVitX(); break; // Pour aller à gauche;
			            case SDLK_RIGHT: joueur2.augVitX(); break; // Pour aller à droite.

			            /* Si l'on appuie sur la touche d'échappement, on souhaite faire apparaître 
			            le menu de pause. */
						case SDLK_ESCAPE: 
							int j = afficherMenuSecondaire(ecran, fenetreAirHockey);
							if(j == 2) // 2 correspond au bouton Quitter.
								jeuEnCours = false; 
							if(j == 1){ // 1 correspond au bouton Menu Principal.
								int k = afficherMenuPrincipal(ecran, fenetreAirHockey);
								if(k == 0){ // L'utilisateur demande à recommencer une partie.
									// Il s'agit juste de réinitialiser les positions et scores.
									joueur1.initScore(); 
									joueur2.initScore();
									joueur1.initPos(33, ECRAN_LARGEUR/2);
									joueur2.initPos(607, ECRAN_LARGEUR/2);
									palet.initPosition(319.5, ECRAN_LARGEUR/2, 0, 0);
									// On remet la musique du menu principal.
                                    //musique=Mix_LoadMUS("../sons/menup2.mp3");
                                    //Mix_PlayMusic(musique,-1);
								}
								if(k == 3) // L'utilisateur clique sur Quitter.
									jeuEnCours = false;
							}
							// Dans le cas où l'on demande juste de reprendre le jeu.
                            //Mix_PlayMusic(musique,-1); // On précise de rejouer la musique pendant le jeu.
						break;
					}
				break;

				// Si l'on relâche une touche.
				case SDL_KEYUP:
			        switch( e.key.keysym.sym )
			        {
			            /* Pour le joueur 1. 
			            Cette fois-ci, si l'on relaĉhe les touches pour le déplacement,
			            on remet les vitesses du joueur à 0.*/
			            case SDLK_z: joueur1.resVitY(); break;
			            case SDLK_s: joueur1.resVitY(); break;
			            case SDLK_q: joueur1.resVitX(); break;
			            case SDLK_d: joueur1.resVitX(); break;
			            // Même chose pour le joueur 2.
			            case SDLK_UP: joueur2.resVitY(); break;
			            case SDLK_DOWN:joueur2.resVitY(); break;
			            case SDLK_LEFT: joueur2.resVitX(); break;
			            case SDLK_RIGHT: joueur2.resVitX(); break;
			        }
			    break;
			}
		}


		/* *** LOGIQUE *** */


		/* *** DEPLACEMENTS *** */

		// Le paramètre barre permet aux deux joueurs de rester dans leurs zones.
		joueur1.deplacer(barre); 
		joueur2.deplacer(barre);
		palet.deplacer(joueur1.retStruct(), joueur2.retStruct());


		/* *** GESTION DES SCORES *** */

		// On incrémente les scores s'il y a but.
		switch(palet.goal()){
			case 1:
			/* On joue l'effet sonore lié à un but. Le 1er argument est un "channel" libre,
			le second l'effet sonore, le 3ème le nombre de répétitions + 1. */
			Mix_PlayChannel(-1,goal,0); 
			//SDL_BlitSurface(imgGoal, NULL, ecran, NULL); // On copie l'image qui doit s'afficher lors d'un but.
			//SDL_UpdateWindowSurface(fenetreAirHockey); // On rafraichit l'écran.
			//SDL_Delay(1000); // On attend une seconde.
			//SDL_FreeSurface(imgGoal); // On enlève l'image.
			joueur1.incScore(); // Incrémentation du score du joueur 1.
			// Initialisation des positions.
			palet.initPosition(319.5, ECRAN_LARGEUR/2, 0, 0); 
			joueur1.initPos(33, ECRAN_LARGEUR/2);
			joueur2.initPos(607, ECRAN_LARGEUR/2);
			break;

			case 2:
			Mix_PlayChannel(-1,goal,0); // De même que précédemment.
			joueur2.incScore();// Incrémentation du score du joueur 2.
			// Initialisation des positions.
			palet.initPosition(319.5, ECRAN_LARGEUR/2, 0, 0);
			joueur1.initPos(33, ECRAN_LARGEUR/2);
			joueur2.initPos(607, ECRAN_LARGEUR/2);
			break;
		}


		/* *** RENDUS *** */

		// Copie de l'image du terrain à l'écran.
		SDL_BlitSurface(terrain, NULL, ecran, NULL);

		// On affiche les maillets et le palet.
		joueur1.afficher(ecran);
		joueur2.afficher(ecran);
		palet.afficher(ecran);

		// Affichage des scores.
		char c[5]; // Contiendra la valeur du score.
		SDL_Rect posScore = {13,19}; // Là où on affiche le score du joueur 1.
		sprintf(c,"%d", joueur1.Score()); // On écrit le score du joueur 1 dans c.
		SDL_Surface* texte = TTF_RenderText_Solid(police, c, couleur); // On crée une image avec le texte du score.
		SDL_BlitSurface(texte, NULL, ecran, &posScore); // On copie cette image à l'écran.
		posScore.x = 612; // Là où on affiche le score du joueur 2
		posScore.y = 19;
		sprintf(c,"%d", joueur2.Score()); // On écrit le score du joueur 2 dans c.
		texte = TTF_RenderText_Solid(police, c, couleur); // On crée une image avec le texte du score.
		SDL_BlitSurface(texte, NULL, ecran, &posScore); // On copie cette image à l'écran.
		SDL_FreeSurface(texte); // On libère le pointeur texte.

		// Rafraichissment de l'écran.
		SDL_UpdateWindowSurface(fenetreAirHockey);

        /*
        * Suite de la gestion de la fréquence des rendus.
        * Ici, l'appel de SDL_GetTicks correspond à l'instant t2 en ms de la fin de parcours de la boucle.
        * 1000/FPS correspond au temps nécessaire en ms pour afficher une image dans le cadre d'une cadence imposée à 60 images par seconde.
        * Donc, SDL_GetTicks()-start est le temps de parcours de la boucle en ms.
        * On teste alors si ce temps de parcours est inférieure au temps que nous souhaitons imposer.
        * En effet l'ordinateur traite les instructions le plus vite possible. On souhaite imposer notre cadence.
        */
		if(1000/FPS>SDL_GetTicks()-start)
			SDL_Delay(1000/FPS-(SDL_GetTicks()-start));

	}

	/* On est sorti de la boucle principale du jeu.
	On quitte tous les systèmes SDL et on libère les adresses mémoires allouées dynamiquement. */
	SDL_DestroyWindow(fenetreAirHockey);
	fenetreAirHockey = NULL;
	SDL_FreeSurface(terrain);
	//Mix_FreeMusic(musique);
	Mix_FreeChunk(goal);
	Mix_CloseAudio();
	IMG_Quit();
	SDL_Quit();
	TTF_CloseFont(police);
	TTF_Quit();
	return 0;
}
