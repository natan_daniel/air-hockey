#ifndef CERCLE_H
#define CERCLE_H

// structure de type cercle
struct Cercle{
    // coordonnées du centre du cercle
    int x, y;
    //rayon
    int r;
    // vitesse du centre d'inertie dans la base (i,j)
    int vx;
    int vy;
};

#endif