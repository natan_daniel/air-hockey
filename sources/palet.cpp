#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include "palet.h"
#include "constantes.h"
#include "cercle.h"
#include <math.h>

double Palet::distance(int x1, int y1, int x2, int y2){
    int deltaX = x2 - x1;
    int deltaY = y2 - y1;
    return sqrt(deltaX*deltaX + deltaY*deltaY);
}

double Palet::produitScalaire(double x1, double y1, double x2, double y2){
    return x1*x2 + y1*y2;
}

bool Palet::collisionCercle(Cercle& a, Cercle& b)
{
    // somme des rayons
    int sommeRayons = a.r + b.r;

    //
    if( distance(a.x, a.y, b.x, b.y) <= (sommeRayons))
    {
        // il y a eu collision
        return true;
    }

    // sinon
    return false;
}

Palet::Palet(SDL_Surface *img, int X, int Y, int vitX, int vitY){

    // image du palet
	image = img;

    // coordonnées cercle
    cercle.x = X;
    cercle.y = Y;

    //rayon cercle
    cercle.r = PALET_LARGEUR/2;

	//vitesse du cercle
	cercle.vx = vitX;
	cercle.vy = vitY;
}

Palet::~Palet(){

	SDL_FreeSurface(image);
}

void Palet::afficher(SDL_Surface *surf){


    SDL_Rect rect;
    rect.x = cercle.x - PALET_LARGEUR/2;
    rect.y = cercle.y - PALET_LARGEUR/2;
    rect.w = PALET_LONGUEUR;
    rect.h = PALET_LARGEUR;

	// copie de la totalité d'image
	SDL_BlitSurface(image, NULL, surf,&rect);

}

void Palet::deplacer(Cercle &a, Cercle &b){

    // Effets sonore lors d'une collision.
    Mix_OpenAudio(44100,MIX_DEFAULT_FORMAT,2,2048);
    Mix_Chunk *coll = Mix_LoadWAV("../sons/Puck_Sound_4.wav"); // Collision avec un maillet.
    Mix_Chunk *coll1 = Mix_LoadWAV("../sons/Puck_Sound_3.wav"); // Collision avec un mur.

    // Des réels qui nous serviront (coordonnées, rayons, vitesses).
    double xNa, yNa, xTa, yTa, xNb, yNb, xTb, yTb, tmp;
    double r, rx, ry, ra, rb, rax, ray, rbx, rby;
    double vn, vt;

    // On déplace le palet.
    cercle.x += cercle.vx;
    cercle.y += cercle.vy;


    /* *** COLLISIONS ET TRAJECTOIRES *** */

    // INITIALEMENT: PALET SANS VITESSE AU CENTRE DU TERRAIN.
    if(cercle.vx == 0 && cercle.vy == 0){   

        // COLLISION AVEC LE JOUEUR 1.
        if(collisionCercle(cercle,a) && !collisionCercle(cercle,b)){ // le cercle a correpond au joueur 1, le cercle b, le joueur 2.

            Mix_PlayChannel(-1,coll,0); // On joue l'effet sonore.

            // On calcule (xNa,yNa), vecteur normal dirigé vers a, non unitaire, dans la base (i,j)
            xNa = a.x - cercle.x;
            yNa = a.y - cercle.y;

            // On normalise (xNa,yNa).
            tmp = xNa*xNa + yNa*yNa;
            xNa = xNa/sqrt(tmp);
            yNa = yNa/sqrt(tmp);

            // distance de retour arrière
            r = cercle.r + a.r - distance(a.x, a.y, cercle.x, cercle.y); // distance dans la zone de collision

            // coordonnées du vecteur recul dans la base (i,j)
            rx = r*xNa;
            ry = r*yNa;

            // correction de la position du palet
            cercle.x -= rx ;
            cercle.y -= ry ;

            // vitesse du palet
            cercle.vx = xNa*produitScalaire(a.vx, a.vy, xNa, yNa); // Le maillet transmet sa vitesse normale
            cercle.vy = yNa*produitScalaire(a.vx, a.vy, xNa, yNa); // au palet.
        }

        // COLLISION AVEC LE JOUEUR 2.
        else if(collisionCercle(cercle,b) && !collisionCercle(cercle,a)){

            Mix_PlayChannel(-1,coll,0); // On joue l'effet sonore.

            // On calcule (xNb,yNb), vecteur normal dirigé vers b, non unitaire dans la base (i,j).
            xNb = b.x - cercle.x;
            yNb = b.y - cercle.y;

            // On normalise (xNb,yNb).
            tmp = xNb*xNb + yNb*yNb;
            xNb = xNb/sqrt(tmp);
            yNb = yNb/sqrt(tmp);

            // distance de retour arrière
            r = cercle.r + b.r - distance(b.x, b.y, cercle.x, cercle.y); // distance dans la zone de collision

            // coordonnées du vecteur recul dans la base (i,j)
            rx = r*xNb;
            ry = r*yNb;

            // correction de la position du palet
            cercle.x -= rx;
            cercle.y -= ry;

            // vitesse du palet
            cercle.vx = xNb*produitScalaire(b.vx, b.vy, xNb, yNb); // Le maillet transmet sa vitesse normale
            cercle.vy = yNb*produitScalaire(b.vx, b.vy, xNb, yNb); // au palet.
        }

        // COLLISION SIMULTANEE JOUEURS 1 ET 2.
        else if(collisionCercle(cercle,a) && collisionCercle(cercle,b)){

            Mix_PlayChannel(-1,coll,0); // On joue l'effet sonore.

            // (xNa,yNa) vecteur normal dirigé vers a, non unitaire, dans la base (i,j).
            xNa = a.x - cercle.x;
            yNa = a.y - cercle.y;

            // (xNb,yNb) vecteur normal dirigé vers b, non unitaire, dans la base (i,j).
            xNb = b.x - cercle.x;
            yNb = b.y - cercle.y;

            // (xNa,yNa) vecteur normal unitaire, dans la base (i,j)
            tmp = xNa*xNa + yNa*yNa;
            xNa = xNa/sqrt(tmp);
            yNa = yNa/sqrt(tmp);

            // (xNb,yNb) vecteur normal unitaire, dans la base (i,j)
            tmp = xNb*xNb + yNb*yNb;
            xNb = xNb/sqrt(tmp);
            yNb = yNb/sqrt(tmp);

            // distance de retour arrière
            ra = cercle.r + a.r - distance(a.x, a.y, cercle.x, cercle.y); // distance dans la zone de collision
            rb = cercle.r + b.r - distance(b.x, b.y, cercle.x, cercle.y); // distance dans la zone de collision

            // coordonnées du vecteur recul ra dans la base (i,j)
            rax = ra*xNa;
            ray = ra*yNa;
            // coordonnées du vecteur recul rb dans la base (i,j)
            rbx = rb*xNb;
            rby = rb*yNb;

            // correction de la position des maillet
            a.x += rax ;
            a.y += ray ;
            b.x += rbx ;
            b.y += rby ;
        }
    }


    // LE PALET EST EN COURS DE DEPLACEMENT.
    if(cercle.vx != 0 || cercle.vy != 0){

        // COLLISION AVEC LE MUR DE GAUCHE (SANS ZONE DE BUT).
        if( cercle.x - cercle.r < 13 && (cercle.y + cercle.r < 182 || cercle.y - cercle.r > 298) ){
            
            Mix_PlayChannel(-1,coll1,0); // On joue l'effet sonore.
            cercle.x = 13 + cercle.r;
            cercle.vx = -cercle.vx;

            // Si collision avec le maillet a en plus de la collision avec le mur.
            if(collisionCercle(cercle,a)){

                Mix_PlayChannel(-1,coll,0); // On joue l'effet sonore.

                // (xNa,yNa) vecteur normal dirigé vers a, non unitaire, dans la base (i,j)
                xNa = a.x - cercle.x;
                yNa = a.y - cercle.y;

                // (xNa,yNa) vecteur normal unitaire, dans la base (i,j)
                tmp = xNa*xNa + yNa*yNa;
                xNa = xNa/sqrt(tmp);
                yNa = yNa/sqrt(tmp);

                // distance de retour arrière
                ra = cercle.r + a.r - distance(a.x, a.y, cercle.x, cercle.y); // distance dans la zone de collision

                // coordonnées du vecteur recul ra dans la base (i,j)
                rax = ra*xNa;
                ray = ra*yNa;

                // correction de la position du maillet
                a.x += rax ;
                a.y += ray ;
            }
        }

        // COLLISION AVEC LE MUR DE DROITE (SANS ZONE DE BUT).
        if( cercle.x + cercle.r > 627 && (cercle.y + cercle.r < 182 || cercle.y - cercle.r > 298) ){
        
            Mix_PlayChannel(-1,coll1,0); // On joue l'effet sonore.
            cercle.x = 627 - cercle.r;
            cercle.vx = -cercle.vx;

            // Si collision avec le maillet b en plus de la collision avec le mur.
            if(collisionCercle(cercle,b)){

                Mix_PlayChannel(-1,coll,0); // On joue l'effet sonore.

                // (xNb,yNb) vecteur normal dirigé vers a, non unitaire, dans la base (i,j)
                xNb = b.x - cercle.x;
                yNb = b.y - cercle.y;

                // (xNb,yNb) vecteur normal unitaire, dans la base (i,j)
                tmp = xNb*xNb + yNb*yNb;
                xNb = xNb/sqrt(tmp);
                yNb = yNb/sqrt(tmp);

                // distance de retour arrière
                rb = cercle.r + b.r - distance(b.x, b.y, cercle.x, cercle.y); // distance dans la zone de collision

                // coordonnées du vecteur recul ra dans la base (i,j)
                rbx = rb*xNb;
                rby = rb*yNb;

                // correction de la position du maillet
                b.x += rbx ;
                b.y += rby ;
            }
        }

        // COLLISION AVEC LE MUR DU HAUT.
        if(cercle.y - cercle.r < 19){

            Mix_PlayChannel(-1,coll1,0); // On joue l'effet sonore.
            cercle.y = 19 + cercle.r;
            cercle.vy = -cercle.vy;

            // Si collision avec le maillet a en plus de la collision avec le mur.
            if(collisionCercle(cercle,a)){

                Mix_PlayChannel(-1,coll,0); // On joue l'effet sonore.

                // (xNa,yNa) vecteur normal dirigé vers a, non unitaire, dans la base (i,j)
                xNa = a.x - cercle.x;
                yNa = a.y - cercle.y;

                // (xNa,yNa) vecteur normal unitaire, dans la base (i,j)
                tmp = xNa*xNa + yNa*yNa;
                xNa = xNa/sqrt(tmp);
                yNa = yNa/sqrt(tmp);

                // distance de retour arrière
                ra = cercle.r + a.r - distance(a.x, a.y, cercle.x, cercle.y); // distance dans la zone de collision

                // coordonnées du vecteur recul ra dans la base (i,j)
                rax = ra*xNa;
                ray = ra*yNa;

                // correction de la position du maillet
                a.x += rax ;
                a.y += ray ;
            }

            // Si collision avec le maillet b en plus de la collision avec le mur.
            if(collisionCercle(cercle,b)){

                Mix_PlayChannel(-1,coll,0); // On joue l'effet sonore.

                // (xNb,yNb) vecteur normal dirigé vers a, non unitaire, dans la base (i,j)
                xNb = b.x - cercle.x;
                yNb = b.y - cercle.y;

                // (xNb,yNb) vecteur normal unitaire, dans la base (i,j)
                tmp = xNb*xNb + yNb*yNb;
                xNb = xNb/sqrt(tmp);
                yNb = yNb/sqrt(tmp);

                // distance de retour arrière
                rb = cercle.r + b.r - distance(b.x, b.y, cercle.x, cercle.y); // distance dans la zone de collision

                // coordonnées du vecteur recul ra dans la base (i,j)
                rbx = rb*xNb;
                rby = rb*yNb;

                // correction de la position du maillet
                b.x += rbx ;
                b.y += rby ;
            }
        }

        // COLLISION AVEC LE MUR DU BAS.
        if(cercle.y + cercle.r > 461){

            Mix_PlayChannel(-1,coll1    ,0); // On joue l'effet sonore.
            cercle.y = 461 - cercle.r;
            cercle.vy = -cercle.vy;

            // Si collision avec le maillet a en plus de la collision avec le mur.
            if(collisionCercle(cercle,a)){

                Mix_PlayChannel(-1,coll,0); // On joue l'effet sonore.

                // (xNa,yNa) vecteur normal dirigé vers a, non unitaire, dans la base (i,j)
                xNa = a.x - cercle.x;
                yNa = a.y - cercle.y;

                // (xNa,yNa) vecteur normal unitaire, dans la base (i,j)
                tmp = xNa*xNa + yNa*yNa;
                xNa = xNa/sqrt(tmp);
                yNa = yNa/sqrt(tmp);

                // distance de retour arrière
                ra = cercle.r + a.r - distance(a.x, a.y, cercle.x, cercle.y); // distance dans la zone de collision

                // coordonnées du vecteur recul ra dans la base (i,j)
                rax = ra*xNa;
                ray = ra*yNa;

                // correction de la position des raquettes
                a.x += rax ;
                a.y += ray ;
            }

            // Si collision avec le maillet b en plus de la collision avec le mur.
            if(collisionCercle(cercle,b)){

                Mix_PlayChannel(-1,coll,0); // On joue l'effet sonore.

                // (xNb,yNb) vecteur normal dirigé vers a, non unitaire, dans la base (i,j)
                xNb = b.x - cercle.x;
                yNb = b.y - cercle.y;

                // (xNb,yNb) vecteur normal unitaire, dans la base (i,j)
                tmp = xNb*xNb + yNb*yNb;
                xNb = xNb/sqrt(tmp);
                yNb = yNb/sqrt(tmp);

                // distance de retour arrière
                rb = cercle.r + b.r - distance(b.x, b.y, cercle.x, cercle.y); // distance dans la zone de collision

                // coordonnées du vecteur recul ra dans la base (i,j)
                rbx = rb*xNb;
                rby = rb*yNb;

                // correction de la position des raquettes
                b.x += rbx ;
                b.y += rby ;
            }
        }

        /* *** *** A AMELIORER *** *** */

        // COLLISION AVEC LE JOUEUR 1.
        if(collisionCercle(cercle,a) && !collisionCercle(cercle,b)){

            Mix_PlayChannel(-1,coll,0); // On joue l'effet sonore.

            // (xNa,yNa) vecteur normal dirigé vers a, non unitaire, dans la base (i,j)
            xNa = a.x - cercle.x;
            yNa = a.y - cercle.y;

            // (xNa,yNa) vecteur normal unitaire, dans la base (i,j)
            tmp = xNa*xNa + yNa*yNa;
            xNa = xNa/sqrt(tmp);
            yNa = yNa/sqrt(tmp);

            //(xTa,yTa) vecteur tangent unitaire dans la base (i,j)
            xTa = -yNa;
            yTa = xNa;

            // distance de retour arrière
            r = cercle.r + a.r - distance(a.x, a.y, cercle.x, cercle.y); // distance dans la zone de collision

            // coordonnées du vecteur recul dans la base (i,j)
            rx = r*xNa;
            ry = r*yNa;

            // correction de la position du palet, rmq on a toujours collision...
            cercle.x -= rx;
            cercle.y -= ry;

            // vitesse du palet
            // Cas où le maillet est immobile
            if(a.vx == 0 && a.vy == 00){
                vt = produitScalaire(cercle.vx, cercle.vy, xTa, yTa);
                vn = produitScalaire(cercle.vx, cercle.vy, xNa, yNa);
                cercle.vx = -vn*xNa + vt*xTa;
                cercle.vy = -vn*yNa + vt*yTa;
            }
            else{
                cercle.vx = xNa*produitScalaire(a.vx, a.vy, xNa, yNa)+produitScalaire(cercle.vx, cercle.vy, xTa, yTa)*xTa;
                cercle.vy = yNa*produitScalaire(a.vx, a.vy, xNa, yNa)+produitScalaire(cercle.vx, cercle.vy, xTa, yTa)*yTa;
            }

            // Si ensuite le maillet b rentre en collision avec le palet alors que le maillet a est déjà en collision avec le palet.
            if(collisionCercle(cercle,a) && collisionCercle(cercle,b)){

                // (xNa,yNa) vecteur normal dirigé vers a, non unitaire, dans la base (i,j)
                xNa = a.x - cercle.x;
                yNa = a.y - cercle.y;

                // (xNb,yNb) vecteur normal dirigé vers a, non unitaire
                xNb = b.x - cercle.x;
                yNb = b.y - cercle.y;

                // (xNa,yNa) vecteur normal unitaire, dans la base (i,j)
                tmp = xNa*xNa + yNa*yNa;
                xNa = xNa/sqrt(tmp);
                yNa = yNa/sqrt(tmp);

                // (xNb,yNb) vecteur normal unitaire, dans la base (i,j)
                tmp = xNb*xNb + yNb*yNb;
                xNb = xNb/sqrt(tmp);
                yNb = yNb/sqrt(tmp);

                // distance de retour arrière
                ra = cercle.r + a.r - distance(a.x, a.y, cercle.x, cercle.y); // distance dans la zone de collision
                rb = cercle.r + b.r - distance(b.x, b.y, cercle.x, cercle.y); // distance dans la zone de collision

                // coordonnées du vecteur recul ra dans la base (i,j)
                rax = ra*xNa;
                ray = ra*yNa;
                // coordonnées du vecteur recul rb dans la base (i,j)
                rbx = rb*xNb;
                rby = rb*yNb;

                // correction de la position des raquettes
                a.x += rax ;
                a.y += ray ;
                b.x += rbx ;
                b.y += rby ;
            }
        }

        /* collision exclusive palet maillet b */
        if(collisionCercle(cercle,b) && !collisionCercle(cercle,a)){

            Mix_PlayChannel(-1,coll,0); // On joue l'effet sonore.

            // (xNb,yNb) vecteur normal dirigé vers b, non unitaire, dans la base (i,j)
            xNb = b.x - cercle.x;
            yNb = b.y - cercle.y;

            // (xNb,yNb) vecteur normal unitaire, dans la base (i,j)
            tmp = xNb*xNb + yNb*yNb;
            xNb = xNb/sqrt(tmp);
            yNb = yNb/sqrt(tmp);

            //(xTb,yTb) vecteur tangent unitaire dans la base (i,j)
            xTb = -yNb;
            yTb = xNb;

            // distance de retour arrière
            r = cercle.r + b.r - distance(b.x, b.y, cercle.x, cercle.y); // distance dans la zone de collision

            // coordonnées du vecteur recul dans la base (i,j)
            rx = r*xNb;
            ry = r*yNb;

            // correction de la position du palet, rmq on a toujours collision...
            cercle.x -= rx;
            cercle.y -= ry;

            //vitesse du palet
            // Cas où le maillet est immobile
            if(b.vx == 0 && b.vy == 00){
                vt = produitScalaire(cercle.vx, cercle.vy, xTb, yTb);
                vn = produitScalaire(cercle.vx, cercle.vy, xNb, yNb);
                cercle.vx = -vn*xNb + vt*xTb;
                cercle.vy = -vn*yNb + vt*yTb;
            }
            else{
                cercle.vx = xNb*produitScalaire(b.vx, b.vy, xNb, yNb)+produitScalaire(cercle.vx, cercle.vy, xTb, yTb)*xTb;
                cercle.vy = yNb*produitScalaire(b.vx, b.vy, xNb, yNb)+produitScalaire(cercle.vx, cercle.vy, xTb, yTb)*yTb;
            }

            // Si ensuite le maillet a rentre en collision avec le palet alors que le maillet b est déjà en collision avec le palet.
            if(collisionCercle(cercle,a) && collisionCercle(cercle,b)){

                // (xNa,yNa) vecteur normal dirigé vers a, non unitaire, dans la base (i,j)
                xNa = a.x - cercle.x;
                yNa = a.y - cercle.y;

                // (xNb,yNb) vecteur normal dirigé vers a, non unitaire
                xNb = b.x - cercle.x;
                yNb = b.y - cercle.y;

                // (xNa,yNa) vecteur normal unitaire, dans la base (i,j)
                tmp = xNa*xNa + yNa*yNa;
                xNa = xNa/sqrt(tmp);
                yNa = yNa/sqrt(tmp);

                // (xNb,yNb) vecteur normal unitaire, dans la base (i,j)
                tmp = xNb*xNb + yNb*yNb;
                xNb = xNb/sqrt(tmp);
                yNb = yNb/sqrt(tmp);

                // distance de retour arrière
                ra = cercle.r + a.r - distance(a.x, a.y, cercle.x, cercle.y); // distance dans la zone de collision
                rb = cercle.r + b.r - distance(b.x, b.y, cercle.x, cercle.y); // distance dans la zone de collision

                // coordonnées du vecteur recul ra dans la base (i,j)
                rax = ra*xNa;
                ray = ra*yNa;
                // coordonnées du vecteur recul rb dans la base (i,j)
                rbx = rb*xNb;
                rby = rb*yNb;

                // correction de la position des raquettes
                a.x += rax ;
                a.y += ray ;
                b.x += rbx ;
                b.y += rby ;
            }
        }
    }
}

int Palet::goal(){
    if(cercle.x + cercle.r < 0)
        return 2;
    if(cercle.x - cercle.r > 640)
        return 1;
    return 0;
}

void Palet::initPosition(int X, int Y, int vitX, int vitY){

    // coordonnées cercle
    cercle.x = X;
    cercle.y = Y;

    //vitesse du cercle
    cercle.vx = vitX;
    cercle.vy = vitY;
}
