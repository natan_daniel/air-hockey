#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "charger.h"

SDL_Surface* chargerImage(SDL_Surface* ecran, const char* c)
{
	SDL_Surface* img = NULL; // Surface qui contiendra l'image finale.

	SDL_Surface* tmp = IMG_Load(c); // On charge l'image fournie en paramètre au sein d'une surface temporaire.
	
	img = SDL_ConvertSurface(tmp,ecran->format,0); // Converison du format de tmp en bits au format de l'écran.

	SDL_SetColorKey(img, SDL_TRUE, SDL_MapRGB(img->format,0x17,0xb3,0x31)); // Couleur à rendre transparent: le vert 17b331 en héxadécimal.

	SDL_FreeSurface(tmp); // On libère la mémoire allouée dynamiquement à tmp;

	return img; // On renvoie la surface qui contient notre image.
}
