#ifndef MAILLET_H
#define MAILLET_H
#include <SDL2/SDL.h>
#include "constantes.h"
#include "cercle.h"

class Maillet{
	private:
		// Le maillet est un cercle.
		Cercle cercle;

        // Il sera représenté à l'écran par une image correspondante à sa forme géométrique.
        SDL_Surface *image;

        // Le score associé au maillet.
        int score;

    public:
        // Constructeur: initialise les membres privés de la classe Maillet.
        Maillet(SDL_Surface *img, int X, int Y);

        // Destructeur: libère image, le pointeur sur SDL_Surface dans la partie privée.
        ~Maillet();

        // Gère les mouvements du maillet.
        void deplacer(SDL_Rect& rec);

        // Les fonctions suivantes gèrent la vitesse du maillet.
        void augVitX(){
        	cercle.vx = MAILLET_VIT;
        }
        void augVitY(){
        	cercle.vy = MAILLET_VIT;
        }
        void dimVitX(){
        	cercle.vx = -MAILLET_VIT ;
        }
        void dimVitY(){
        	cercle.vy = -MAILLET_VIT ;
        }
        void resVitX(){
        	cercle.vx = 0;
        }

        void resVitY(){
        	cercle.vy = 0;
        }

        // Détecte la collision avec un rectangle.
        bool collision(SDL_Rect& rec);

        // Gère l'affichage du maillet à l'écran.
        void afficher(SDL_Surface *surf);

        // Renvoie la structure du maillet.
        Cercle& retStruct(){
            return cercle;
        }

        // Incrémente le score.
        void incScore(){
        	score++;
        }

        // Renvoie la valeur du score.
        int Score(){
        	return score;
        }

        // Initialise le score à 0.
        void initScore(){
        	score = 0;
        }

        // Initialise la position du maillet.
        void initPos(int X, int Y){
        	cercle.x = X;
        	cercle.y = Y;
        }
};

#endif
