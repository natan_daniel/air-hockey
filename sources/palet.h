#ifndef PALET_H
#define PALET_H
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include "constantes.h"
#include "cercle.h"


// Je réunis au sein d'une classe tout concernant le palet.
class Palet
{
        // image du palet
        SDL_Surface *image;

        // cercle qui contient le palet (mouvement et collision)
        Cercle cercle;

        double distance( int x1, int y1, int x2, int y2 );

        double produitScalaire(double x1, double y1, double x2, double y2);

        bool collisionCercle(Cercle& a, Cercle& b);

    public:

        // constructeur: initialise les variables
        Palet(SDL_Surface *img, int X, int Y, int vitX, int vitY);

        // destructeur: libère la mémoire allouée dynamiquement
        ~Palet();

        // déplacements
        void deplacer(Cercle &a, Cercle &b);

        // affichage
        void afficher(SDL_Surface *surf);

        // récupère les inormations sur le cercle du palet
        Cercle& retournerCercle(){
            return cercle;
        }

        int goal();

        // pour réinitialiser la position
        void initPosition(int X, int Y, int vitX, int vitY);
};



#endif
