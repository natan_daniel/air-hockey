#ifndef CONSTANTES_H
#define CONSTANTES_H

// dimensions de la fenêtre de jeu
const int ECRAN_LONGUEUR = 640;
const int ECRAN_LARGEUR = 480;

// MAILLET
const int MAILLET_COTE = 40;  // Le maillet est un cercle dans un carré de côté 40 pixels.
const int MAILLET_VIT = 7.5;	  // Vitesse maximale du maillet selon (Ox) ou (Oy).

// dimensions palet
const int PALET_LONGUEUR = 20;
const int PALET_LARGEUR = 20;

// dimensions boutons
const int BOUTON_LONGUEUR = 245;
const int BOUTON_LARGEUR = 80;

#endif