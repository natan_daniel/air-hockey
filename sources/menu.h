#ifndef MENU_H
#define MENU_H
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "charger.h"

/** \brief Affiche le menu principal et permet de naviguer dans les sous menus options et instructions.
 * \param ecran SDL_Surface* �cran de travail.
 * \param fenetreAirHockey SDL_Window* fen�tre de travail.
 * \return int 
 * Cette fonction fait un appel � la fonction menuPrincipal qui affiche le menu principal.
 * Elle permet aussi de naviguer dans les sous menus options et instructions si l'on clique sur les boutons associ�s.
 * Elle lance une musique.
 * Elle renvoie 0 ou 3 selon que l'on d�cide de jouer ou de quitter le jeu.
 * La valeur de retour est interpr�t�e dans la fonction principal main.
 */
int afficherMenuPrincipal(SDL_Surface* ecran, SDL_Window* fenetreAirHockey);

/** \brief Affiche le menu secondaire et permet le retour au menu principal.
 * \param ecran SDL_Surface* �cran de travail.
 * \param fenetreAirHockey SDL_Window* fen�tre de travail.
 * \return int 
 * Cette fonction fait appel � la fonction menuSecondaire qui affiche le menu de pause.
 * On peut reprendre le jeu, le quitter ou revenir au menu principal.
 * Cette fonction lance une musique aussi.
 * Elle renvoie 0 (reprendre), 1 (menu principal) ou 2 (quitter).
 * La valeur de retour est interpr�t�e dans la fonction principal main.
 */
int afficherMenuSecondaire(SDL_Surface* ecran, SDL_Window* fenetreAirHockey);

 /** \brief MenuPrincipal affiche les diff�rents �l�ments du menu principal du jeu.
* \param ecran SDL_Surface* ecran de travail.
* \param fenetreAirHockey SDL_Window* fen�tre de travail.
* \return int
* La fonction charge et copie les images des boutons et de l'arri�re plan sur l'�cran de la fen�tre de jeu.
* Puis, elle les affiche.
* Elle renvoie le num�ro du bouton sur lequel on a cliqu� (entre 0 et 3).
*/
int MenuPrincipal(SDL_Surface* ecran, SDL_Window* fenetreAirHockey);

/** \brief MenuSecondaire affiche les diff�rents �l�ments du menu de pause.
* \param ecran SDL_Surface* ecran de travail.
* \param fenetreAirHockey SDL_Window* fen�tre de travail.
* \return int
* La fonction charge et copie les images des boutons et de l'arri�re plan sur l'�cran de la fen�tre de jeu.
* Puis, elle les affiche.
* Elle renvoie le num�ro du bouton sur lequel on a cliqu� (entre 0 et 2).
*/
int MenuSecondaire(SDL_Surface* ecran, SDL_Window* fenetreAirHockey);

#endif
