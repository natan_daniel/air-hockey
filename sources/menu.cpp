#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include "charger.h"
#include "menu.h"
#include "constantes.h"

bool instructionsDemandees = false; // Drapeau qui gère l'entrée ou non dans le sous menu instructions.
bool optionsDemandees = false; // Drapeau qui gère l'entrée ou non dans le sous menu options.

int MenuPrincipal(SDL_Surface* ecran, SDL_Window* fenetreAirHockey){

    
    /* Gestion de la fréquence des rendus. */

	const int FPS = 60; // On impose une cadence de 60 images par seconde.
	Uint32 t1; // Cette variable (entier non signé sur 32 bits) marquera l'instant de départ de la boucle du menu principal.

    /* Coordonnées de la souris. */

	int x,y;

	/* Menu principal. */

	const int NBBOUTONS=4; // Nombre de boutons, soit 4.
	SDL_Surface* boutons[NBBOUTONS]; // Tableau qui contiendra les images pour chague bouton.
	boutons[0]=chargerImage(ecran, "../graphismes/boutons/Jouer.png"); // On charge les images des boutons au sein du tableau.
	boutons[1]=chargerImage(ecran, "../graphismes/boutons/Options.png");
	boutons[2]=chargerImage(ecran, "../graphismes/boutons/Instructions.png");
	boutons[3]=chargerImage(ecran, "../graphismes/boutons/Quitter.png");

	SDL_Rect position[NBBOUTONS]; // Tableau qui contiendra les positions (des rectangles) pour chaque bouton.
	for(int j = 0; j < NBBOUTONS ; j++){  // On initialise les positions des boutons.
		position[j].x = 60;
		position[j].y = 120 + 80*j;
		position[j].w = BOUTON_LONGUEUR;
		position[j].h = BOUTON_LARGEUR;
	}

	SDL_Surface* accueil = chargerImage(ecran, "../graphismes/fonds/accueil.png"); // On charge l'image d'arrière plan.
	SDL_BlitSurface(accueil, NULL, ecran, NULL); // On copie l'image d'arrière plan à l'écran.
    for(int j = 0; j< NBBOUTONS; j++) // On copie les images des boutons à l'écran.
		SDL_BlitSurface(boutons[j], NULL, ecran, &position[j]);
	SDL_UpdateWindowSurface(fenetreAirHockey); // Enfin, on met à jour l'écran pour effectivement voir les images copiées.


	/* Boucle infinie du menu  principal. */

	SDL_Event e; // e récupère un événement (appui sur une touche, un clique de la souris, etc) de la file d'attente des événements.

	while(1){

		t1=SDL_GetTicks();  // On marque l'instant en ms auquel on rentre dans cette boucle.

		// On reste dans cette boucle tant qu'il y a un événement dans la file d'attente.
		while(SDL_PollEvent(&e)){ // On extrait tour à tour les événements dans la file d'attente. Priorité FIFO.

			switch(e.type){ // On effectue des tests selon la nature de l'événement.

				case SDL_QUIT: // On a cliqué sur la croix de fermeture de la fenêtre.

					for(int j = 0; j < NBBOUTONS; j++)
						SDL_FreeSurface(boutons[j]);  // On désalloue la mémoire accordée aux boutons, pointeurs sur des surfaces.
					return 3; // On renvoie la valeur 3 qui sera interprêtée dans le main.

				break;

				case SDL_MOUSEBUTTONDOWN: // On a cliqué avec la souris.

					SDL_GetMouseState(&x, &y); // On récupère les coordoonées de la souris.

					for(int j = 0; j < NBBOUTONS; j++){
						// Si la souris est au dessus d'un bouton...
						if(x>=position[j].x && x<=position[j].x + position[j].w && y>=position[j].y && y<=position[j].y + position[j].h){
							for(int k = 0; k < NBBOUTONS; k++){
								SDL_FreeSurface(boutons[k]); // ...on désalloue la mémoire accordée aux boutons, pointeurs sur des surfaces, et...
								return j; // ...on renvoie le numéro du bouton.
							}
						}
					}
				break;
			}
		}

        /*
        * Suite de la gestion de la fréquence des rendus.
        * Ici, l'appel de SDL_GetTicks correspond à l'instant t2 de la fin de parcours de la boucle du menu.
        * 1000/FPS correspond au temps nécessaire pour afficher une image dans le cadre d'une cadence imposée à 60 images par seconde.
        * Donc, SDL_GetTicks()-t1 est le temps de parcours de la boucle en millisecondes.
        * On teste alors si ce temps de parcours est inférieure au temps que nous souhaitons imposer.
        */
       
		if(1000/FPS>SDL_GetTicks()-t1)
			SDL_Delay(1000/FPS-(SDL_GetTicks()-t1)); // Si nous avons de l'avance, on attend le temps nécessaire.
	}
}

int MenuSecondaire(SDL_Surface* ecran, SDL_Window* fenetreAirHockey){

    /* Gestion de la fréquence des rendus. */
  
	const int FPS = 60; // On impose une cadence de 60 images par seconde.
	Uint32 t1; // Cette variable (entier non signé sur 32 bits) marquera l'instant de départ de la boucle du menu secondaire.

  
    /* Coordonnées de la souris. */
	int x,y;

	/* Menu secondaire. */
	const int NBBOUTONS=3; // Nombre de boutons, soit 3.
	SDL_Surface* boutons[NBBOUTONS]; // Tableau qui contiendra les images pour chague bouton.
	boutons[0]=chargerImage(ecran, "../graphismes/boutons/Reprendre.png"); // On charge les images des boutons au sein du tableau.
	boutons[1]=chargerImage(ecran, "../graphismes/boutons/menuprincipal.png");
	boutons[2]=chargerImage(ecran, "../graphismes/boutons/Quitter.png");

	SDL_Rect position[NBBOUTONS]; // Tableau qui contiendra les positions (des rectangles) pour chaque bouton.
	for(int j = 0; j < NBBOUTONS ; j++){  // On initialise les positions des boutons.
		position[j].x = 45	;
		position[j].y = 150 + 90*j ;
		position[j].w = BOUTON_LONGUEUR;
		position[j].h = BOUTON_LARGEUR;
	}

	SDL_Surface* bkg = chargerImage(ecran, "../graphismes/fonds/pause.png"); // On charge l'image d'arrière plan.
	SDL_BlitSurface(bkg, NULL, ecran, NULL); // On copie l'image d'arrière plan à l'écran.
	for(int j = 0; j < NBBOUTONS; j++) // On copie les images des boutons à l'écran.
		SDL_BlitSurface(boutons[j], NULL, ecran, &position[j]);
	SDL_UpdateWindowSurface(fenetreAirHockey); // Enfin, on met à jour l'écran pour effectivement voir les images copiées.


	/* Boucle infinie du menu secondaire. */ 

	SDL_Event e; // e récupère un événement (appui sur une touche, un clique de la souris, etc) de la file d'attente des événements.

	while(1){

		t1=SDL_GetTicks();  // On marque l'instant en ms auquel on rentre dans cette boucle.

		// On reste dans cette boucle tant qu'il y a un événement dans la file d'attente.
		while(SDL_PollEvent(&e)){ // On extrait tour à tour les événements dans la file d'attente. Priorité FIFO.

			switch(e.type){ // On effectue des tests selon la nature de l'événement.

				case SDL_QUIT: // On a cliqué sur la croix de fermeture de la fenêtre.

					for(int j=0; j < NBBOUTONS; j++)
						SDL_FreeSurface(boutons[j]);// On désalloue la mémoire accordée aux boutons, pointeurs sur des surfaces.
					return 2; // On renvoie la valeur 2 qui sera interprêtée dans le main.
				break;

				case SDL_MOUSEBUTTONDOWN: // On a cliqué avec la souris.

					SDL_GetMouseState(&x, &y); // On récupère les coordoonées de la souris.

					for(int j = 0; j < NBBOUTONS; j++){
						// Si la souris est au dessus d'un bouton...
						if(x>=position[j].x && x<=position[j].x + position[j].w && y>=position[j].y && y<=position[j].y + position[j].h){
							for(int k = 0; k < NBBOUTONS; k++){
								SDL_FreeSurface(boutons[k]); // ...on désalloue la mémoire accordée aux boutons, pointeurs sur des surfaces, et...
								return j; // ...on renvoie le numéro du bouton.
							}
						}
					}
				break;
			}
		}

        /*
        * Suite de la gestion de la fréquence des rendus.
        * Ici, l'appel de SDL_GetTicks correspond à l'instant t2 de la fin de parcours de la boucle du menu.
        * 1000/FPS correspond au temps nécessaire pour afficher une image dans le cadre d'une cadence imposée à 60 images par seconde.
        * Donc, SDL_GetTicks()-t1 est le temps de parcours de la boucle en millisecondes.
        * On teste alors si ce temps de parcours est inférieure au temps que nous souhaitons imposer.
        */
       
		if(1000/FPS>SDL_GetTicks()-t1)
			SDL_Delay(1000/FPS-(SDL_GetTicks()-t1)); // Si nous avons de l'avance, on attend le temps nécessaire.
	}
}


int afficherMenuPrincipal(SDL_Surface* ecran, SDL_Window* fenetreAirHockey){

	SDL_Event e; // Pour gérer les événements.
    int i; // Contiendra la valeur à renvoyer.

    /* Musique du menu principal. */
   
    Mix_Music *musique; // Cette variable stocke notre fichier mp3.
    musique=Mix_LoadMUS("../sons/menup2.mp3"); // On charge la musique souhaitée.
    Mix_PlayMusic(musique,-1); // On décide de la jouer à l'infinie grâce à cette fonction et l'argument -1.

    /* Boucle de navigation dans le menu principal. */
   
	do{
			i = MenuPrincipal(ecran, fenetreAirHockey); // Affichage du menu principal. On met dans i le numéro du bouton sur lequel on clique

			if(i == 1){ // Test si on demande le sous menu des options.

				optionsDemandees = true; // Drapeau d'accès mis à true.

   				 /* Coordonnées de la souris. */
				int x,y;	


				/* *** BOUTONS SONS *** */

				const int NBBOUTONS=2; // Nombre de boutons, soit 2.
				SDL_Surface* boutons[NBBOUTONS]; // Tableau qui contiendra les images pour chague bouton.
				boutons[0]=chargerImage(ecran, "../graphismes/boutons/sonon.png"); // On charge les images des boutons au sein du tableau.
				boutons[1]=chargerImage(ecran, "../graphismes/boutons/sonoff.png");

				SDL_Rect position[NBBOUTONS]; // Tableau qui contiendra les positions (des rectangles) pour chaque bouton.
				for(int j = 0; j < NBBOUTONS ; j++){  // On initialise les positions des boutons.
					position[j].x = 80 + 255*j;
					position[j].y = 150;
					position[j].w = BOUTON_LONGUEUR;
					position[j].h = BOUTON_LARGEUR;
				}

				// On copie l'image de fond de ce sous menu.
				SDL_Surface* options = chargerImage(ecran, "../graphismes/fonds/Options.png");
				SDL_BlitSurface(options, NULL, ecran, NULL);
				for(int j = 0; j < NBBOUTONS; j++) // On copie les images des boutons à l'écran.
					SDL_BlitSurface(boutons[j], NULL, ecran, &position[j]);
				SDL_UpdateWindowSurface(fenetreAirHockey); // On rafraichit l'écran.

				// Boucle du sous menu options.
				while(optionsDemandees){

                    // Gestion de la fréquence des rendus.
                    const int FPS = 60;
					Uint32 t1 = SDL_GetTicks(); // On marque l'instant d'entrée dans cette boucle du menu.

					// On reste dans cette boucle tant qu'il y a un événement dans la file d'attente.
					while(SDL_PollEvent(&e)){ // On extrait tour à tour les événements dans la file d'attente. Priorité FIFO.

						switch(e.type){ // On effectue des tests selon la nature de l'événement.

							case SDL_QUIT: // On a cliqué sur la croix de fermeture de la fenêtre.
								SDL_FreeSurface(options); // On désalloue la mémoire accordée à options, pointeur sur surface.
								for(int j = 0; j < NBBOUTONS; j++)
									SDL_FreeSurface(boutons[j]);  // On désalloue la mémoire accordée aux boutons, pointeurs sur des surfaces.
								optionsDemandees = false; // On met la valeur de notre drapeau à false pour sortir de la boucle.
								i = 3; // On renvoie la valeur 3 qui sera interprêtée dans le main.
							break;

							case SDL_KEYDOWN: // On a appuyé sur une touche.

								if(e.key.keysym.sym == SDLK_ESCAPE){ // Test pour vérifier si c'est la touche d'échappement.
									SDL_FreeSurface(options); // On désalloue la mémoire accordée à options, pointeur sur surface.
									for(int j = 0; j < NBBOUTONS; j++)
										SDL_FreeSurface(boutons[j]);  // On désalloue la mémoire accordée aux boutons, pointeurs sur des surfaces.
									optionsDemandees = false; // On met la valeur de notre drapeau à false pour sortir de la boucle.
								}
							break;

							case SDL_MOUSEBUTTONDOWN: // On a cliqué avec la souris.

							SDL_GetMouseState(&x, &y); // On récupère les coordoonées de la souris.

							// Bouton son off
							if(x>=position[1].x && x<=position[1].x + position[1].w && y>=position[1].y && y<=position[1].y + position[1].h){
									Mix_PauseMusic();
							}
							// Bouton son on.
							if(x>=position[0].x && x<=position[0].x + position[0].w && y>=position[0].y && y<=position[0].y + position[0].h){
									Mix_ResumeMusic();
							}
							break;
						}
					}

                    /*
                    * Suite de la gestion de la fréquence des rendus.
                    * Ici, l'appel de SDL_GetTicks correspond à l'instant t2 de la fin de parcours de la boucle du menu.
                    * 1000/FPS correspond au temps nécessaire pour afficher une image dans le cadre d'une cadence imposée à 60 images par seconde.
                    * Donc, SDL_GetTicks()-t1 est le temps de parcours de la boucle en millisecondes.
                    * On teste alors si ce temps de parcours est inférieure au temps que nous souhaitons imposer.
                    */

                    if(1000/FPS>SDL_GetTicks()-t1)
                        SDL_Delay(1000/FPS-(SDL_GetTicks()-t1)); // Si nous avons de l'avance, on attend le temps nécessaire.
				}
			}

			if(i == 2){ // Test si on demande le sous menu des instructions.

				instructionsDemandees = true; // Drapeau d'accès mis à true.

				// Boucle du sous menu instructions.
				while(instructionsDemandees){

                    // Gestion de la fréquence des rendus.
                    const int FPS = 60;
					Uint32 t1 = SDL_GetTicks(); // On marque l'instant d'entrée dans cette boucle du menu.

                    // On copie l'image de fond de ce sous menu et on actualise l'écran.
					SDL_Surface* instructions = chargerImage(ecran, "../graphismes/fonds/instructions.jpg");
					SDL_BlitSurface(instructions, NULL, ecran, NULL);
					SDL_UpdateWindowSurface(fenetreAirHockey);
					
					// On reste dans cette boucle tant qu'il y a un événement dans la file d'attente.
					while(SDL_PollEvent(&e)){ // On extrait tour à tour les événements dans la file d'attente. Priorité FIFO.

						switch(e.type){ // On effectue des tests selon la nature de l'événement.
							case SDL_QUIT: // On a cliqué sur la croix de fermeture de la fenêtre.
								SDL_FreeSurface(instructions); // On désalloue la mémoire accordée à instructions, pointeur sur surface.
								instructionsDemandees = false;// On met la valeur de notre drapeau à false pour sortir de la boucle.
								i = 3; // On renvoie la valeur 3 qui sera interprêtée dans le main.
							break;

							case SDL_KEYDOWN: // On a appuyé sur une touche.

								if(e.key.keysym.sym == SDLK_ESCAPE){ // Test pour vérifier si c'est la touche d'échappement.
									SDL_FreeSurface(instructions); // On désalloue la mémoire accordée à instructions, pointeur sur surface.
									instructionsDemandees = false; // On met la valeur de notre drapeau à false pour sortir de la boucle.
								}
							break;
						}
					}

                    /*
                    * Suite de la gestion de la fréquence des rendus.
                    * Ici, l'appel de SDL_GetTicks correspond à l'instant t2 de la fin de parcours de la boucle du menu.
                    * 1000/FPS correspond au temps nécessaire pour afficher une image dans le cadre d'une cadence imposée à 60 images par seconde.
                    * Donc, SDL_GetTicks()-t1 est le temps de parcours de la boucle en millisecondes.
                    * On teste alors si ce temps de parcours est inférieure au temps que nous souhaitons imposer.
                    */

                    if(1000/FPS>SDL_GetTicks()-t1)
                        SDL_Delay(1000/FPS-(SDL_GetTicks()-t1)); // Si nous avons de l'avance, on attend le temps nécessaire.
                }
			}

	}while(i != 0 && i != 3);

	Mix_FreeMusic(musique); // On arrête la musique.

	return i; // On renvoie la valeur 0 ou 3, jouer ou quitter le jeu.
}

int afficherMenuSecondaire(SDL_Surface* ecran, SDL_Window* fenetreAirHockey){

    int i; // Contiendra la valeur de retour.

	do{

			i = MenuSecondaire(ecran, fenetreAirHockey);

	}while(i != 0 && i != 2 && i!=1);

	return i;
}
