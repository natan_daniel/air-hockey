#include <SDL2/SDL.h>
#include "maillet.h"
#include "constantes.h"

Maillet::Maillet(SDL_Surface *img, int X, int Y)
{
    // L'image du maillet.
    image = img;

    // Coordonnées du centre d'inertie du cercle qui représente le maillet.
    cercle.x = X;
    cercle.y = Y;

    // Rayon du cercle qui représente le maillet.
    cercle.r = MAILLET_COTE/2;

    // Vitesse initiale du maillet.
    cercle.vx = 0;
    cercle.vy = 0;

    // Score initiale du maillet.
    score = 0;
}

Maillet::~Maillet(){
    // Libère la mémoire allouée à image à la fin de l'exécution du programme.
    SDL_FreeSurface(image);
}

void Maillet::deplacer(SDL_Rect& rec){
    // Déplacements verticaux du maillet.
    cercle.y += cercle.vy;
    // Si collision, on ajuste la position du maillet.
    if(cercle.y - cercle.r < 19) // limite supérieure à 19 px
        cercle.y = 19 + cercle.r;
    if(cercle.y + cercle.r > 461) // limite inférieure à 460 px
        cercle.y = 461 - cercle.r;

    // Déplacements horizontaux du maillet.
    // Si le maillet est du côté gauche.
    if(cercle.x + cercle.r <= 315){
        cercle.x += cercle.vx;
        if(cercle.x - cercle.r < 13)  // collision avec le mur de gauche à 13 pixels
            cercle.x = 13 + cercle.r;
        if(collision(rec)) // rec permet de définir la barrière invisible au milieu du terrain.
            cercle.x = 315 - cercle.r;  // bloquage du maillet au millieu du terrain côté gauche
    }
    // Si le maillet est du côté droit.
    if(cercle.x - cercle.r >= 324){
        cercle.x += cercle.vx;
        if(cercle.x + cercle.r >= 627) // collision avec le mur de droite à 627 pixels
            cercle.x = 627 - cercle.r;
        if(collision(rec))
            cercle.x = 324 + cercle.r; // bloquage du maillet au milieu du terrain côté droit
    }
}

bool Maillet::collision(SDL_Rect& rec){
    if(cercle.x + cercle.r < rec.x)
        return false;
    if(cercle.x - cercle.r > rec.x + rec.w)
        return false;
    return true;
}

void Maillet::afficher(SDL_Surface *surf){
    SDL_Rect rect;
    rect.x = cercle.x - MAILLET_COTE/2;
    rect.y = cercle.y - MAILLET_COTE/2;
    rect.w = MAILLET_COTE;
    rect.h = MAILLET_COTE;

    // copie de la totalité d'image
    SDL_BlitSurface(image, NULL, surf,&rect);
}









