#ifndef CHARGER_H
#define CHARGER_H
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

/**
* \brief Permet de charger une image au sein d'une surface.
* \param ecran SDL_Surface* écran de travail.
* \param c const char* adresse de l'image que l'on souhaite charger.
* Charge une image au sein d'une surface en effectuant une conversion du format de celui-ci au format de l'écran.
* En effet, ecran est une surface et possède un certain (32 bits par exemple). 
* Le format de l'image que l'on veut charger peut être différent.
* On effectue alors une conversion ici. Cette conversion aurait eu lieu lors de la copie de l'image sur l'écran.
* On évite alors d'effectuer la converion à chaque fois que l'on fait une copie (blit).
* On renvoie la surface qui contient l'image.
*/
SDL_Surface* chargerImage(SDL_Surface* ecran, const char* c);

#endif
